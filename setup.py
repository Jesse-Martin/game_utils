from setuptools import setup

setup(
    name='game_utils',
    version='2020.10.6',
    packages=['game_utils', 'game_utils.map_utils', 'game_utils.map_utils.tile_map_utils', 'game_utils.sprite_utils',
              'game_utils.character_utils', "game_utils.scene_utils"],
    url='',
    license='',
    author='Jesse Martin',
    author_email='',
    description='Simple Utility Library For Pygame'
)
