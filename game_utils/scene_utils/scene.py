import pygame
from abc import *


class AbstractScene(ABC):

    def __init__(self, size=(500, 500), surface=None):
        self.background = (0, 0, 0)
        self.size = size
        if surface is None:
            self.surface = pygame.Surface(size)

    @abstractmethod
    def load(self):
        pass

    @abstractmethod
    def unload(self):
        pass

    @abstractmethod
    def update(self, delta_time):
        pass

    @abstractmethod
    def draw(self, surface):
        pass


class SubScene(AbstractScene, ABC):

    def __init__(self, top_left, size):
        self.top_left = top_left
        super().__init__(size)




class CompoundScene(AbstractScene, ABC):

    def __init__(self, scenes: list, size):
        super().__init__(size)
        self.scene_list = scenes

    def update(self, delta_time):
        for scene in self.scene_list:
            scene.draw(delta_time)

    def draw(self, surface):

        for scene in self.scene_list:
            sub_surface = pygame.Surface(scene.size)
            scene.draw(sub_surface)
            surface.blit(sub_surface, scene.top_left)
