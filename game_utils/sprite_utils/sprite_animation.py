import pygame


class SpriteAnimation(pygame.sprite.Sprite):

    def __init__(self, sprite_sheet, key_frames: list, times: list, *args):
        super().__init__(*args)
        self._sprite_sheet = sprite_sheet
        self._key_frames = key_frames
        self._times = times
        self._frame_index = 0
        self.animation_time = 0
        self.total_frames = len(self._key_frames)
        self.current_frame = self._sprite_sheet.get_frame(self._frame_index)
        self.rect = self.current_frame.get_rect()
        self.image = self.current_frame
        self.raw_image = self.current_frame
        self.forward = True
        self.loop = False
        self._play = False
        self.rotation = 0

    def play(self):
        self._play = True

    def pause(self):
        self._play = False

    def stop(self):
        self._play = False
        self._frame_index = 0
        self.animation_time = 0
        key_frame = self._key_frames[self._frame_index]
        self.raw_image = self._sprite_sheet.get_frame(key_frame)

    def reset(self):
        self._frame_index = 0 if self.forward else self.total_frames - 1
        self.animation_time = 0 if self.forward else self._times[self._frame_index]

    def frame_time_elapsed(self):
        if self.forward:
            return self._times[self._frame_index] <= self.animation_time

        return self._times[self._frame_index - 1] >= self.animation_time

    def set_next_frame(self):
        self._frame_index += 1 if self.forward else -1

        if 0 > self._frame_index or self._frame_index >= self.total_frames:
            if self.loop:
                self.reset()
            else:
                self.stop()
                return

        key_frame = self._key_frames[self._frame_index]
        self.raw_image = self._sprite_sheet.get_frame(key_frame)

    def update(self, delta_time):
        start_index = self._frame_index
        if self._play:
            self.animation_time += delta_time if self.forward else -delta_time

            if self.frame_time_elapsed():
                self.set_next_frame()

        self.image = pygame.transform.rotate(self.raw_image, self.rotation)
        #if start_index != self._frame_index:
