import pygame


class SpriteSheet(pygame.sprite.Sprite):

    def __init__(self, image, width, height, f_width, f_height, *args):
        super().__init__(*args)
        self.image = image
        self.rect = self.image.get_rect()
        self.frames = []
        self.image_size = (width, height)
        self.frame_size = (f_width, f_height)
        self.frame_count = int((width * height) / (f_width * f_height))
        self.__create_frames(self.image_size, self.frame_size)

    @staticmethod
    def load(path, width, height, f_width, f_height):
        image = pygame.image.load(path)
        return SpriteSheet(image, width, height, f_width, f_height)

    def __create_frames(self, image_size,  frame_size):
        max_width_count = int(image_size[0] / frame_size[0])
        max_height_count = int(image_size[1] / frame_size[1])
        for j in range(max_height_count):
            for i in range(max_width_count):
                rect = (i * frame_size[0], j * frame_size[1], frame_size[0], frame_size[1])
                self.frames.append(self.image.subsurface(rect))

    def get_frame(self, frame_index):
        return self.frames[frame_index]
