import pygame
from abc import *
from game_utils.scene_utils.scene import AbstractScene


class Game(AbstractScene, ABC):

    def __init__(self, size=[500, 500]):
        super().__init__(size)
        self._event_handlers = {}
        self.running = True
        self.register_event_handler(pygame.QUIT, self._quit)
        self.tick_time = 60
        self.background = (255, 255, 255)
        self.sprite_groups = []
        self.size = size

    def _quit(self, event):
        self.running = False

    def register_event_handler(self, key, handler):
        self._event_handlers[key] = handler

    def get_event_handler(self, key):
        if key in self._event_handlers.keys():
            return self._event_handlers[key]

        return None

    def unregister_event_handler(self, key):
        if key in self._event_handlers.keys():
            del self._event_handlers[key]

    def event_handler(self):
        for evt in pygame.event.get():
            if evt.type in self._event_handlers.keys():
                self._event_handlers[evt.type](evt)

    def run(self):
        pygame.init()
        screen = pygame.display.set_mode(self.size)
        self.surface = screen
        self.load()
        clock = pygame.time.Clock()

        accumulator = 0
        while self.running:
            clock.tick(self.tick_time)
            self.event_handler()
            delta_time = clock.get_time()
            accumulator += delta_time
            while accumulator > self.tick_time:
                accumulator -= self.tick_time
                self.update(delta_time)

            screen.fill(self.background)
            self.draw(screen)
            pygame.display.flip()

        self.unload()
        pygame.quit()
