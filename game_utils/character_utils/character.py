import pygame


class Character(pygame.sprite.Sprite):

    def __init__(self, animations: list, start_point: tuple, *args):
        super().__init__(*args)
        self.animations = animations
        self.point = start_point
        self.current_animation = 0
        self.rect.center = start_point
        self.last_rect = self.rect
        self.rotation = 0
        self.velocity = []

    @property
    def image(self):
        return self.animations[self.current_animation].image

    @property
    def rect(self):
        return self.animations[self.current_animation].rect

    @rect.setter
    def rect(self, value):
        self.animations[self.current_animation].rect = value

    def set_rotation(self, rotation):
        self.animations[self.current_animation].rotation = rotation

    def update(self, delta_time):
        self.set_rotation(self.rotation)
        self.animations[self.current_animation].update(delta_time)
        # -rotation because for some reason it works that way
        speed = pygame.math.Vector2(*self.velocity).rotate(-self.rotation)
        self.rect = self.rect.move(speed)
        self.last_rect = self.rect
