import pygame


class Tile(pygame.sprite.Sprite):

    def __init__(self, image, poss, size, *args):
        super().__init__(*args)
        self.image = image
        self.rect = pygame.Rect(poss, size)
