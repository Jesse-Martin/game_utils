import pygame
import game_utils


class HalfScreen(game_utils.SubScene):

    def __init__(self, top_left, size):
        super().__init__(top_left ,size)

    def load(self):
        pass

    def unload(self):
        pass

    def update(self, delta_time):
        pass

    def draw(self, surface):
        surface.fill(self.background)


class SplitScreen(game_utils.CompoundScene):

    def load(self):
        a = HalfScreen((0,0),[500, 400])
        a.background = (0,255,0)
        b = HalfScreen([0, 400], [500, 100])
        b.background = (255,255,0)
        self.scene_list.append(a)
        self.scene_list.append(b)

    def unload(self):
        pass


class MyGame(game_utils.Game):

    def load(self):
        self.background = (0, 0, 0)
        self.ss = SplitScreen([], (500, 500))
        self.ss.load()

    def unload(self):
        pass

    def update(self, delta_time):
        pass

    def draw(self, screen):
        self.ss.draw(screen)


if __name__ == "__main__":
    g = MyGame()
    g.run()
