from game_utils import SpriteSheet
from game_utils import Game
from game_utils.map_utils.tile_map_utils import *
import pygame


class MyTile(Tile):

    def player_collision(self, player):
        pass

    def arrow_collision(self, arrow):
        pass

    def add_arrow(self, arrow):
        pass


class IceTile(Tile):

    def player_collision(self, player):
        player.last_tile = self



class SnowTile(Tile):

    def player_collision(self, player):
        # Stop the player
        player.stop()
        # Move the player to the center of the last valid tile
        player.reset_tile()

    def arrow_collision(self, arrow):
        arrow.stop()
        tile.add_arrow(arrow)


class RockTile(SnowTile):

    def player_collision(self, player):
        super().player_collision(player, tile)
        player.trip()
        player.lose_life()


class WaterTile(SnowTile):

    def player_collision(self, player):
        super().player_collision(player, tile)
        player.sink()
        player.lose_life()

    def arrow_collision(self, arrow):
        pass

class IceMap(TileMap):

    IceCharacter = '.'
    RockCharacter = 'r'
    SnowCharacter = 's'
    WaterCharacter = 'w'
    TargetCharacter = 't'
    PlayerSpawn = 'p'

    DefaultTile = IceTile

    def __init__(self, *tiles):
        super().__init__(*tiles)

    @staticmethod
    def load( map_file):
        with open(map_file) as f:
            data = f.read()
        tiles = []
        tile_sheet = SpriteSheet.load("assets/WinterTileSet.png", 64, 64, 32, 32)
        size = 32
        x, y = (0, 0)
        for c in data:
            x_pos = size * x
            if c == "\n":
                x = 0
                y += 1
            else:
                x_pos = size * x
                y_pos = size * y
                if IceMap.RockCharacter == c:
                    tile_type = RockTile
                    index = 1
                elif IceMap.WaterCharacter == c:
                    tile_type = WaterTile
                    index = 3
                elif IceMap.SnowCharacter == c:
                    tile_type = SnowTile
                    index = 0
                else:
                    tile_type = IceTile
                    index = 2

                terrain_image = tile_sheet.get_frame(index).copy()
                tile = tile_type(terrain_image, [x_pos, y_pos], [size, size])
                tiles.append(tile)
                if c == 't':
                    tile.target = True

                x += 1
        return IceMap(*tiles)



class MyGame(Game):

    def __init__(self, *args):
        super().__init__(*args)
        self.background = (0, 0, 0)
        self.register_event_handler(pygame.KEYDOWN, self.keypress_handler)
        self.levels = [
            'assets/Level1.lvl',
            'assets/Level2.lvl',
            'assets/Level3.lvl',
            'assets/Level4.lvl',
            'assets/Level5.lvl',
            'assets/Level6.lvl',
            'assets/Level7.lvl',
            'assets/Level8.lvl',
            'assets/Level9.lvl',
            'assets/Level10.lvl'
        ]
        self.level = 0

    def keypress_handler(self, key):
        print(key)
        if key.unicode == "+":
            if self.level + 1 < len(self.levels):
                self.level += 1
                self.load()
        if key.unicode == "-":
            if self.level - 1 >= 0:
                self.level -= 1
                self.load()

    def load(self):
        self.terrain_group = IceMap.load(self.levels[self.level])

    def unload(self):
        pass

    def update(self, delta_time):
        pass

    def draw(self, screen):
        self.terrain_group.draw(screen)


if __name__ == "__main__":
    print(pygame.K_PLUS)
    game = MyGame()
    game.run()
