import game_utils
import pygame


class MyCharacter(game_utils.Character):

    def __init__(self, animations: list, start_point: tuple, *args):
        super().__init__(animations, start_point, *args)
        self.up = False
        self.down = False
        self.left = False
        self.right = False
        self.rotate_left = False
        self.rotate_right = False

    def keypress_handler(self, evt):
        # if evt.key == pygame.K_0:
        #     temp_rect = self.last_rect
        #     self.animations[self.current_animation].stop()
        #     self.current_animation = 0
        #     self.animations[self.current_animation].play()
        #     self.rect = temp_rect
        #     self.set_rotation(self.rotation)
        # elif evt.key == pygame.K_1:
        #     temp_rect = self.last_rect
        #     #temp_roat = self.rotation
        #     self.animations[self.current_animation].stop()
        #     self.current_animation = 1
        #     self.animations[self.current_animation].play()
        #     self.rect = temp_rect
        #     self.set_rotation(self.rotation)
        #     #self.animations[self.current_animation].rotation

        if evt.key == pygame.K_w:
            self.up = True
        elif evt.key == pygame.K_s:
            self.down = True
        elif evt.key == pygame.K_a:
            self.left = True
        elif evt.key == pygame.K_d:
            self.right = True
        elif evt.key == pygame.K_q:
            self.rotate_left = True
        elif evt.key == pygame.K_e:
            self.rotate_right = True


    def keyrelease_handler(self, evt):
        if evt.key == pygame.K_w:
            self.up = False
        elif evt.key == pygame.K_s:
            self.down = False
        elif evt.key == pygame.K_a:
            self.left = False
        elif evt.key == pygame.K_d:
            self.right = False
        elif evt.key == pygame.K_q:
            self.rotate_left = False
        elif evt.key == pygame.K_e:
            self.rotate_right = False


    def update(self, delta_time):
        if self.rotate_left:
            self.rotation += 5
        elif self.rotate_right:
            self.rotation -= 5

        x, y = 0, 0
        if self.up:
            y = -5
        elif self.down:
            y = 5

        if self.left:
            x = -5
        elif self.right:
            x = 5

        self.velocity = [x, y]

        super().update(delta_time)





class MyGame(game_utils.Game):

    def load(self):
        self.background = (0, 0, 0)
        # sheet = game_utils.SpriteSheet.load("assets/player.png", 400, 444, 50, 37)
        sheet = game_utils.SpriteSheet.load("assets/SnowBall.png", 48, 12, 12, 12)
        # idle1_animation = game_utils.SpriteAnimation(sheet, [0, 1, 2, 3], [50, 100, 150, 200])
        idle1_animation = game_utils.SpriteAnimation(sheet, [0, 1, 2, 3], [100, 200, 300, 400, 450])
        # idle2_animation = game_utils.SpriteAnimation(sheet, [38, 39, 40, 41], [50, 100, 150, 200])
        idle1_animation.loop = True
        # idle2_animation.loop = True
        # idle1_animation.play()
        idle1_animation.play()
        # idle2_animation.play()

        ani_list = [idle1_animation]
        c = MyCharacter(ani_list, (250, 250))
        self.character_group = pygame.sprite.Group([c])
        self.register_event_handler(pygame.KEYDOWN, c.keypress_handler)
        self.register_event_handler(pygame.KEYUP, c.keyrelease_handler)

    def unload(self):
        pass

    def draw(self, screen):
        self.character_group.draw(screen)

    def update(self, delta_time):
        self.character_group.update(delta_time)


if __name__ == "__main__":
    game = MyGame()
    game.run()
